package controllers;

import play.*;
import play.mvc.*;
import java.util.*;

import models.*;

public class DonationController extends Controller {
	
	/**
	 * Method to render donate html.Rendering User, donation 
	 * progress and total donations to page.
	 */
    public static void donate() {
		User user = Accounts.getCurrentUser();
		if(user == null){
		    Logger.info("Donation class : Unable to getCurrentUser");
		    Accounts.login();
		}
		else{
			String prog = getPercentTargetAchieved();
			String progress = prog + "%";
			long pro = getDonations();
			Logger.info("Donation ctrler : percent target achieved "+ progress);
			render(user, progress, pro);
		}
	}
    
    /**
     * Method to render renderReport html.
     * Rendering all donations to page.
     */
    public static void renderReport(){
    	User user = Accounts.getCurrentUser();
		if(user == null){
		    Logger.info("Donation class : Unable to getCurrentUser");
		    Accounts.login();
		}
		else{
		    List<Donation> donations = Donation.findAll();
		    render(donations);
		}
    }

    /**
     * Method to take in donation values(current user, amount and method), create donation 
     * object by calling addDonation.
     * 
     * @param amountDonated
     * @param methodDonated
     */
	public static void donates(long amountDonated, String methodDonated){
		
		Logger.info("amount donated: " + amountDonated + " method donated: "+ methodDonated);
		
		User user = Accounts.getCurrentUser();
		if(user == null){
			Logger.info("Donation class : Unable to getCurrentUser");
		    Accounts.login();
		}
		else{
			addDonation(user, amountDonated, methodDonated);
	    }
		donate();
	}
	
	/**
	 * Method to create a donation object and save it to the data base.
	 * 
	 * @param user
	 * @param amountDonated
	 * @param methodDonated
	 */
	private static void addDonation(User user, long amountDonated, String methodDonated){
		
		Donation bal = new Donation(user, amountDonated, methodDonated);
		bal.save();
	}
	
	/**
	 * Method to return target donation value.
	 * 
	 * @return long
	 */
    private static long getDonationTarget(){
		
		return 20000;
	}
	
    /**
     * Method to return string value of percent of target achieved.
     * 
     * @return String
     */
	public static String getPercentTargetAchieved(){
		long total = getDonations();
		long target = getDonationTarget();
		long percentachieved = (total * 100 / target);
		String progress = String.valueOf(percentachieved);
		return progress;
	} 
	
	/**
	 * Method to return total of  all donations in database.
	 * 
	 * @return long
	 */
	public static long getDonations(){
		List<Donation> donations = Donation.findAll();
		long total = 0;
		for(Donation d : donations){
			total += d.received;
		}
		return total;
	}
}
	
	
	
