package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Welcome extends Controller {

	/**
	 * Method to render Welcome index html/landing page.
	 */
    public static void index() {
        render();
    }
}