package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

public class Accounts extends Controller {

	/**
	 * Method to render Welcome html page.
	 */
    public static void index() {
        render();
    }
    
    /**
     * Method to render signup html.
     */
    public static void signup() {
	    render();
	}
    
    /**
     * Method to render login html.
     */
    public static void login() { 
	    render();
	}

    /**
     * Method to logout user(clear session map) and render Welcome html.
     */
	public static void logout() {
	      session.clear();
	      index();
	}
	
	/**
	 * Method to register new User object and save to database.
	 * 
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param password
	 * @param USACITIZEN
	 */
	public static void register(String firstName, String lastName, String email,String password,  boolean USACITIZEN) {
	    Logger.info(firstName + " " + lastName + " " + email + " " + USACITIZEN + " " + password);
	    User user = new User(firstName, lastName, email, password,  USACITIZEN);
	    user.save();
	    Accounts.login();
	 }
	
	/**
	 * Method to check login details of user, checking email and password
	 * against values in database, rendering login if incorrect or donate html
	 * if successful.
	 * 
	 * @param email
	 * @param password
	 */
	 public static void authenticate(String email, String password) {
	    Logger.info("Attempting to authenticate with " + email + ":" +  password);

	    User user = User.findByEmail(email);
	    if ((user != null) && (user.checkPassword(password) == true)){
	      Logger.info("Authentication successful");
	      session.put("logged_in_userid", user.id);
	      DonationController.donate();
	    }
	    else{
	      Logger.info("Authentication failed");
	      login();  
	    }
	 }
	 
	 /**
	  * Method to find and return logged in user.
	  * 
	  * @return User
	  */
     public static User getCurrentUser(){
		 String userId = session.get("logged_in_userid");
		 if(userId == null){
			 return null;
		 }
		 User logged_in_user = User.findById(Long.parseLong(userId));
		 Logger.info("Logged in user is "+ logged_in_user.firstName);
		 return logged_in_user;	 
	 }
}