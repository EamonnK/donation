package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import play.db.jpa.Blob;
import play.db.jpa.Model;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

@Entity
public class Donation extends Model
{
	//fields that make Donation class
	public long received;
	public String methodDonated;
	public Date donatedAt;
	//foreign key, part of donation object, user who makes donation
	@ManyToOne
	public User from;
	
	/**
	 * Constructor for objects of class Donation.
	 * 
	 * @param from
	 * @param received
	 * @param methodDonated
	 */
	public Donation(User from, long received, String methodDonated){
		this.received = received;
		this.methodDonated = methodDonated;
		this.from = from;
		donatedAt = new Date();
	}
}