package models;

import javax.persistence.Entity;
import play.db.jpa.Blob;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import javax.persistence.OneToMany;

import controllers.Accounts;
import play.db.jpa.Model;

@Entity
public class User extends Model
{
  //fields that make user class
  public String firstName;
  public String lastName;
  public String email;
  public String password;
  public boolean USACITIZEN;
  
  /**
   * Constructor for objects of User class.
   * 
   * @param firstName
   * @param lastName
   * @param email
   * @param password
   * @param USACITIZEN
   */
  public User(String firstName, String lastName, String email, String password, boolean USACITIZEN )
  {
    this.firstName   = firstName;
    this.lastName    = lastName;
    this.email       = email;
    this.password    = password;
    this.USACITIZEN  = USACITIZEN;
  }
  
  /**
   * Method to find user by email.
   * 
   * @param email
   * @return User
   */
  public static User findByEmail(String email)
  {
    return find("email", email).first();
  }

  /**
   * Method to check input password against stored value.
   * 
   * @param password
   * @return
   */
  public boolean checkPassword(String password)
  {
    return this.password.equals(password);
  }
  
 
  
  

}